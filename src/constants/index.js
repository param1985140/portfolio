import filmfiesta from '../assets/projects/filmfiesta.png'
import purityplants from '../assets/projects/purityplants.png'
import toggl from '../assets/projects/toggl.png'
import blog from '../assets/projects/blog-cms.jpg'

export const HERO_CONTENT = `I am a passionate front-end developer eager to craft engaging and responsive web applications. With a solid foundation in modern web technologies, I have developed skills in React.js through personal projects and internships. My enthusiasm for creating intuitive user interfaces drives me to continually learn and adapt to new technologies and best practices. I aim to leverage my growing expertise to contribute to innovative solutions that enhance user experiences and support business objectives.`;

export const ABOUT_TEXT = `I am a dedicated developer with a strong interest in React JS and a solid foundation in PHP and MySQL. My journey in web development has led me to explore and master React, allowing me to create dynamic and interactive user interfaces. Alongside my development skills, I have delved into automation testing, gaining proficiency in tools and frameworks such as Selenium in Java, Cucumber, TestNG, Cypress, and BDD architecture. My expertise extends to both UI and API testing, ensuring robust and reliable applications. I am excited to leverage my skills in development and testing to contribute to innovative projects and deliver exceptional user experiences.`;

export const EXPERIENCES = [
  {
    period: "October 2023 - November 2023",
    role: "Web developer Intern",
    company: "Octanet Services Pvt Ltd",
    description: `Designed a landing page for their website and completed few tasks related to web developement during the coursework`,
    technologies: ["HTML","CSS", "Javascript", "ReactJs"],
  },
  {
    period: "June 2023 - Present",
    role: "Editorial Lead",
    company: "Rotaract Club of Ulhasnagar Sapna Garden",
    description: `Designed bulletin for the club , wrote monthly blogs related to various topics , led a team being a leader or the Chairperson in various bigger events`,
    technologies: ["Leadership","Teamwork","Public speaking" , "Presentations"],
  },
 
];

export const PROJECTS = [
  {
    title: "Filmfiesta",
    image: filmfiesta,
    description:
      "A platform where users can search for movies and features included such as darkmode and loghtmode toggling , search for movies , imdb ratings for the movies , etc",
    technologies: ["HTML", "CSS", "React", "Tailwind", "API"],
  },
  {
    title: "Purity Plants",
    image: purityplants,
    description:
      "A fully fledged e-commerce website for users to buy variety of plants where project includes backend simulation with authorization",
    technologies: ["HTML", "CSS","Tailwind", "React", "Reducers", "Context APIs", "JSON-server"],
  },
  {
    title: "Toggl",
    image: toggl,
    description:
      "A Simple web application that help users to track their project time and can set price/hr according to the project",
    technologies: ["HTML", "CSS", "React", "Bootstrap","Materialized UI"],
  },
  {
    title: "Blog CMS",
    image: blog,
    description:
      "A platform for creating and publishing blog posts, with features like posting the blog , updating , deleting the blogs , etc",
    technologies: ["HTML", "CSS", "PHP", "Laravel", "mySQL"],
  },
];

export const CONTACT = {
  address: "318,5,Near BK Smart Mall,Ulhasnagar-421002",
  phoneNo: "+91 8669851313 ",
  email: "charhaparam@gmail.com.com",
};
