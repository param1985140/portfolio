import React from 'react'
import { FaLinkedin } from 'react-icons/fa'
import { FaSquareXTwitter } from 'react-icons/fa6'
import { FaInstagram } from 'react-icons/fa'
import { FaGitlab } from 'react-icons/fa'

export const Header = () => {
  return (
    <nav className="mb-20 flex items-center justify-between py-6">
        <div className="flex flex-shrink-0 items-center">
            <h1 className='text-4xl'>Pc</h1>
        </div>
        <div className='m-8 flex items-center gap-4 text-2xl'>
          <a href="https://www.linkedin.com/in/paramcharha/">
             <FaLinkedin/>
          </a>
          <a href="https://gitlab.com/paramcharha">
            <FaGitlab/>
          </a>
          <a href="https://www.instagram.com/param1903/">
            <FaInstagram/>
          </a>
            <FaSquareXTwitter/>
        </div>
    </nav>
  )
}
